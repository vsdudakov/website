from django.contrib import admin

from item_plugin.models import ItemPlugin


class ItemPluginAdmin(admin.ModelAdmin):
    pass
    
admin.site.register(ItemPlugin, ItemPluginAdmin)
