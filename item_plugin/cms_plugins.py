from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from item_plugin.models import ItemPlugin



class CMSItemPlugin(CMSPluginBase):
    model = ItemPlugin
    render_template = 'item_plugin/default.html'

    def render(self, context, instance, placeholder):
        self.render_template = instance.template_name
        context['instance'] = instance
        return context
    

plugin_pool.register_plugin(CMSItemPlugin) # register the plugin

 
