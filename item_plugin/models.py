# coding: utf-8
from django.db import models
from django.conf import settings

from cms.models import CMSPlugin


class ItemPlugin(CMSPlugin):
    template_name = models.CharField(max_length=255, choices=settings.CMS_ITEM_TEMPLATES)

    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    picture = models.ImageField(upload_to='item_plugin/picture', null=True, blank=True)
    big_picture = models.ImageField(upload_to='item_plugin/big_picture', null=True, blank=True)
    awesome_icon = models.CharField(max_length=255, null=True, blank=True)

    category = models.CharField(max_length=255, null=True, blank=True)
    client = models.CharField(max_length=255, null=True, blank=True)
    date = models.DateTimeField(null=True, blank=True)
    
    twitter = models.CharField(max_length=255, null=True, blank=True)
    facebook = models.CharField(max_length=255, null=True, blank=True)
    vk = models.CharField(max_length=255, null=True, blank=True)
    linkedin = models.CharField(max_length=255, null=True, blank=True)
    
    def __unicode__(self):
        return u'%s - %s' % (self.get_template_name_display(), self.title)

   
        
