from django.db import models

from cms.models import CMSPlugin


class FeedbackPlugin(CMSPlugin):
    email_1 = models.EmailField(max_length=255, null=True, blank=True)
    email_2 = models.EmailField(max_length=255, null=True, blank=True)

    def __unicode__(self):
        return '%s, %s' % (self.email_1, self.email_2)


class FeedbackItem(models.Model):
    plugin_data = models.ForeignKey(FeedbackPlugin)
    name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name
