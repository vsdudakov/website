from django.shortcuts import redirect, get_object_or_404
from django.contrib import messages
from django.http import Http404
from django.utils.translation import ugettext as _ul

from feedback_plugin.models import FeedbackPlugin
from feedback_plugin.forms import FeedbackForm


#http://ivanishchev.ru/
def feedback_view(request, pk):
    context = {}
    form_class = FeedbackForm
    next = request.GET.get('next', '/')

    if request.method == "POST":
        form = form_class(request.POST, plugin_data=get_object_or_404(FeedbackPlugin, pk=pk))
        if form.is_valid():
            form.save()
            messages.success(request, _ul('Success. Your request has been sent to our team. Please waiting our response.'), extra_tags='feedback')
        else:
            messages.error(request, _ul('Error. Your request has not been sent to our team. Please enter all fields correctly.'), extra_tags='feedback')
        return redirect(next)
    raise Http404

