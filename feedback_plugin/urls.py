from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)/$', 'feedback_plugin.views.feedback_view', name='feedback'),
)
