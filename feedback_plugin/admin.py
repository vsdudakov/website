from django.contrib import admin

from feedback_plugin.models import FeedbackItem, FeedbackPlugin


class FeedbackItemAdmin(admin.ModelAdmin):
    pass
admin.site.register(FeedbackItem, FeedbackItemAdmin)


class FeedbackPluginAdmin(admin.ModelAdmin):
    pass
admin.site.register(FeedbackPlugin, FeedbackPluginAdmin)
