from cms.plugin_pool import plugin_pool
from cms.plugin_base import CMSPluginBase

from feedback_plugin.models import FeedbackPlugin
from feedback_plugin.forms import FeedbackForm


class CMSFeedbackPlugin(CMSPluginBase):

    """ 
    model where plugin data are saved module = _("Polls") name = _("Poll Plugin") # name 
    of the plugin in the interface 
    render_template = "djangocms_polls/poll_plugin.html"
    """
    model = FeedbackPlugin
    render_template = "feedback_plugin/feedback_plugin.html"

    def render(self, context, instance, placeholder):
        context['instance'] = instance
        context['feedback_form'] = FeedbackForm(plugin_data=instance)
        return context
    

plugin_pool.register_plugin(CMSFeedbackPlugin) # register the plugin

 
