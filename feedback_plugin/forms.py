# -*- coding: utf-8 -*-

from django import forms
from django.utils.translation import ugettext as _ul

from website.utils import form_fields_to_bootstrap

from feedback_plugin.models import FeedbackItem


class FeedbackForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        self.plugin_data = kwargs.pop('plugin_data', None)
        super(FeedbackForm, self).__init__(*args, **kwargs)
        form_fields_to_bootstrap(self.fields)
        self.fields['name'].widget.attrs['placeholder'] = _ul(u'Ваше имя *')
        self.fields['phone'].widget.attrs['placeholder'] = _ul(u'Ваш телефон или email *')
    
    def save(self, *args, **kwargs):
        self.instance.plugin_data = self.plugin_data
        return super(FeedbackForm, self).save(*args, **kwargs)

    class Meta:
        model = FeedbackItem
        fields = ('name', 'phone',)
