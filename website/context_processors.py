# -*- coding: utf-8 -*-
from django.contrib.sites.models import Site


def get_current_site(request):
    context = {}
    context['current_site'] = Site.objects.get_current()
    return context

