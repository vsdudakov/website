

def form_fields_to_bootstrap(fields):
    for key in fields:
        field = fields[key]
        if field.label and field.required:
            field.label += ' *'
        if field.__class__.__name__ in (
                'DateTimeField', 'DateField', 
                'TimeField', 'CharField', 'Textarea',
                'EmailField', 'IntegerField', 'FloatField',
                ):
            field.widget.attrs.update(
                {'class': field.widget.attrs.get('class', '') + ' form-control'}
                )

